# Planets App

This application will show the planets list from the API as well will work from offline once data sync up has done initially.

## Application mets the below requirements

* The application having a screen which displays a list of planet names from the first page of planet data
* Planets has been persisted for offline viewing
* Minimum iOS version will be 11.0
* The app will support both iPhone and iPad as well landscap and portrait mode
* The app has been unit tested
* Code alligned with proper comments
* only loaded first page of data in given web service
* Only used the standard Apple iOS frameworks and doesnt used any third-party libraries
## Screenshots
### Online Mode
![ScreenShot](/screenshots/OnlineMode.png)
![ScreenShot](/screenshots/PlanetsList.png)
### Offline Mode
![ScreenShot](/screenshots/FirstTimeOfflineMode_WithoutSyncUp.png)
![ScreenShot](/screenshots/OfflineMode.png)
![ScreenShot](/screenshots/PlanetsList.png)

## List of recommendations for future features and improvements
* Remove the hardcoded paging values and calculate computed page number to show the planets lists when user scrolling further.
* Add Light weight migration pre requistes in the core data context
* Planets list show show some detailed information for the user due now showing only planet name
* Stilll need some changes in UI part.
* Give some more control options to user application will work always offline except initil sync up.
* Customizing font, color sizings
* Give some sharing options to social media, If required add some notes or marked as favourites.


## Author

* **Arulpandiyan Shanmuganathan** - *Initial work*

