//
//  PlanetsViewModel.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import Foundation

struct PlanetsViewModel {
    let planetName: String
    
    //Mark: View Model
    init(planet: Result) {
        // PlanetName
        planetName = String.emptyIfNil(planet.name)
    }
}
