//
//  PlanetsPresenter.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import Foundation
import CoreData

typealias FetchItemsCompletionBlock = (_ success: Bool, _ error: NSError?) -> Void

// MARK: - PlanetsPresenterProtocol
protocol PlanetsPresenterProtocol {
    var items: [PlanetsViewModel?]? { get }
    var itemCount: Int { get }
    
    func item(at index: Int) -> PlanetsViewModel?
    func fetchItems(withOfflineMode:Bool, _ completionBlock: @escaping FetchItemsCompletionBlock)
}

// MARK: - PlanetsPresenterProtocol methods
extension PlanetsPresenterProtocol {
    var items: [PlanetsViewModel?]? {
        return items
    }
    
    var itemCount: Int {
        return items?.count ?? 0
    }
    
    func item(at index: Int) -> PlanetsViewModel? {
        guard index >= 0 && index < itemCount else { return nil }
        return items?[index] ?? nil
    }
}

// MARK: - PlanetsPresenter

class PlanetsPresenter: PlanetsPresenterProtocol {
    private static let entityName = "Result"
    private let persistentContainer: NSPersistentContainer
    private var fetchItemsCompletionBlock: FetchItemsCompletionBlock?
    
    var items: [PlanetsViewModel?]? = []
    
    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }
    
    func fetchItems(withOfflineMode OfflineMode:Bool, _ completionBlock: @escaping FetchItemsCompletionBlock) {
        fetchItemsCompletionBlock = completionBlock
        if OfflineMode {
            offLineDataFetch()
        } else {
            loadNextPageIfNeeded(for: 1)
        }
    }
    
    func item(at index: Int) -> PlanetsViewModel? {
        return items?[index] ?? nil
    }
    

}

//MARK: - Private methods
private extension PlanetsPresenter {
    
    // MARK: Parse Json data
    
    func parse(_ jsonData: Data) -> Bool {
        do {
            guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
                fatalError("Failed to retrieve managed object context")
            }
            
            // Parse JSON data
            let managedObjectContext = persistentContainer.viewContext
            let decoder = JSONDecoder()
            decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
            _ = try decoder.decode(Planets.self, from: jsonData)
            try managedObjectContext.save()
            return true
        } catch let error {
            print(error)
            return false
        }
    }
    
    // MARK: Fetch data from core data

    func fetchFromStorage() -> [Result]? {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Result>(entityName: PlanetsPresenter.entityName)
        do {
            let planetsObjct = try managedObjectContext.fetch(fetchRequest)
            return planetsObjct
        } catch let error {
            print(error)
            return nil
        }
    }
    
    // MARK: Clear core data

    func clearStorage() {
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: PlanetsPresenter.entityName)
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }

    // MARK: init with model

    static func initViewModels(_ planets: [Result]?) -> [PlanetsViewModel]? {
        if let planetsArray = planets {
            return planetsArray.map { planet in
                    return PlanetsViewModel(planet: planet)
            }
        }  else {
            return nil
    }
        
    }

    //MARK: - Offline data fetch
    
    func offLineDataFetch() {
        if let planets = self.fetchFromStorage() {
            if planets.count > 0 {
                self.items = PlanetsPresenter.initViewModels(planets)
                DispatchQueue.main.async {
                    self.fetchItemsCompletionBlock?(true, nil)
                }
            } else {
                DispatchQueue.main.async {
                    self.fetchItemsCompletionBlock?(false, NSError.createError(0, description: "Please sync the data from server once for offline experience"))
                    return
                }
            }
        }
    }
    
    
    //MARK: - Online data fetch

    func loadNextPageIfNeeded(for index: Int) {
        let urlString = String(format: "https://swapi.co/api/planets/?page=\(index)")
        guard let url = URL(string: urlString) else {
            fetchItemsCompletionBlock?(false, nil)
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            guard let jsonData = data, error == nil else {
                DispatchQueue.main.async {
                    strongSelf.fetchItemsCompletionBlock?(false, error as NSError?)
                }
                return
            }
            strongSelf.clearStorage()
            if strongSelf.parse(jsonData) {
                if let planets = strongSelf.fetchFromStorage() {
                    strongSelf.items = PlanetsPresenter.initViewModels(planets)
                }
                DispatchQueue.main.async {
                    strongSelf.fetchItemsCompletionBlock?(true, nil)
                }
            } else {
                DispatchQueue.main.async {
                    strongSelf.fetchItemsCompletionBlock?(false, NSError.createError(0, description: "JSON parsing error"))
                }
            }
        }
        task.resume()
    }
}

