//
//  LoginViewController.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    var offlineMode: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.offlineMode = false

        // Do any additional setup after loading the view.
    }

    
    //MARK: - IBAction Methods
    @IBAction func switchChangeAction(_ sender: UISwitch) {
        if(sender.isOn) {
            self.offlineMode = true
        } else {
            self.offlineMode = false
        }
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "PlanetsViewIdentifier", sender: self)
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! PlanetsViewController
        viewController.offlineMode = self.offlineMode
        let planetsPresenter = PlanetsPresenter(persistentContainer: (UIApplication.shared.delegate as! AppDelegate).persistentContainer)
        viewController.planetsPresenter = planetsPresenter
    }
    

}
