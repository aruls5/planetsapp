//
//  ViewController.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import UIKit
import CoreData


class PlanetsViewController: UITableViewController {
    var offlineMode: Bool?
    
    
    private static let UserCellReuseId = "PlanetCell"
    
    var planetsPresenter: PlanetsPresenterProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: PlanetsViewController.UserCellReuseId)
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        initializePresenterWithData()
        
    }
    
    //Mark: - Private Methods
    private func initializePresenterWithData(){
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge, color: .gray,  placeInTheCenterOf: view)
        activityIndicator.startAnimating()
        planetsPresenter?.fetchItems(withOfflineMode: self.offlineMode!) { [weak self] (success, error) in
                guard let strongSelf = self else { return }
                if !success {
                    DispatchQueue.main.async {
                        let title = "Error"
                        if let error = error {
                            strongSelf.showError(title, message: error.localizedDescription)
                        } else {
                            strongSelf.showError(title, message: NSLocalizedString("Can't retrieve Planets.", comment: "Can't retrieve Planets."))
                        }
                        activityIndicator.stopAnimating()
                    }
                } else {
                    DispatchQueue.main.async {
                        strongSelf.tableView.reloadData()
                        activityIndicator.stopAnimating()
                    }
                }
    }
    }
    
}

// MARK: UITableViewDataSource

extension PlanetsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planetsPresenter?.itemCount ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PlanetsViewController.UserCellReuseId, for: indexPath)
        
        if let viewModel = planetsPresenter?.item(at: indexPath.row) {
            cell.textLabel?.text = "\(viewModel.planetName)"
        } else {
            cell.textLabel?.text = "???"
        }
        
        return cell
    }
}

// Mark: AlertViewController
extension PlanetsViewController {
    func showError(_ title: String, message: String) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
}
}


// MARK: - Activity Indicator
extension UIActivityIndicatorView {
    convenience init(activityIndicatorStyle: UIActivityIndicatorViewStyle, color: UIColor, placeInTheCenterOf parentView: UIView) {
        self.init(activityIndicatorStyle: activityIndicatorStyle)
        center = parentView.center
        self.color = color
        parentView.addSubview(self)
    }
}

