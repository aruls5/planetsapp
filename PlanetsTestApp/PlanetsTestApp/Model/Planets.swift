//
//  PlanetsModel.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import Foundation
import CoreData

class Planets: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
        case next, previous, results
        case count
    }
    
    @NSManaged var count: Int
    @NSManaged var next: String?
    @NSManaged var previous: String?
    @NSManaged var results: [Result]?

    // MARK: - Decodable
    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Planets", in: managedObjectContext) else {
                fatalError("Failed to decode Planets")
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.count = try container.decodeIfPresent(Int.self, forKey: .count) ?? 0
        self.next = try container.decodeIfPresent(String.self, forKey: .next)
        self.previous = try container.decodeIfPresent(String.self, forKey: .previous)
        _ = try container.decodeIfPresent([Result].self, forKey: .results)
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(count, forKey: .count)
        try container.encode(next, forKey: .next)
        try container.encode(previous, forKey: .previous)
    }

    
}

class Result: NSManagedObject,Codable {
    @NSManaged var name, rotationPeriod, orbitalPeriod, diameter: String?
    @NSManaged var climate, gravity, terrain, surfaceWater: String?
    @NSManaged var population: String?
    @NSManaged var residents, films: [String]?
    @NSManaged var created, edited: String?
    @NSManaged var url: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case rotationPeriod = "rotation_period"
        case orbitalPeriod = "orbital_period"
        case diameter, climate, gravity, terrain
        case surfaceWater = "surface_water"
        case population, residents, films, created, edited, url
    }
    
    // MARK: - Decodable
    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entityResults = NSEntityDescription.entity(forEntityName: "Result", in: managedObjectContext) else {
                fatalError("Failed to decode Results")
        }
        self.init(entity: entityResults, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.rotationPeriod = try container.decodeIfPresent(String.self, forKey: .rotationPeriod)
        self.orbitalPeriod = try container.decodeIfPresent(String.self, forKey: .orbitalPeriod)
        self.diameter = try container.decodeIfPresent(String.self, forKey: .diameter)
        self.climate = try container.decodeIfPresent(String.self, forKey: .climate)
        self.gravity = try container.decodeIfPresent(String.self, forKey: .gravity)
        self.terrain = try container.decodeIfPresent(String.self, forKey: .terrain)
        self.surfaceWater = try container.decodeIfPresent(String.self, forKey: .surfaceWater)
        self.population = try container.decodeIfPresent(String.self, forKey: .population)
        self.residents = try container.decodeIfPresent([String].self, forKey: .residents)
        self.films = try container.decodeIfPresent([String].self, forKey: .films)
        self.created = try container.decodeIfPresent(String.self, forKey: .created)
        self.edited = try container.decodeIfPresent(String.self, forKey: .edited)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
    }
    
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(rotationPeriod, forKey: .rotationPeriod)
        try container.encode(orbitalPeriod, forKey: .orbitalPeriod)
        try container.encode(diameter, forKey: .diameter)
        try container.encode(climate, forKey: .climate)
        try container.encode(gravity, forKey: .gravity)
        try container.encode(terrain, forKey: .terrain)
        try container.encode(surfaceWater, forKey: .surfaceWater)
        try container.encode(population, forKey: .population)
        try container.encode(residents, forKey: .residents)
        try container.encode(films, forKey: .films)
        try container.encode(created, forKey: .created)
        try container.encode(edited, forKey: .edited)
        try container.encode(url, forKey: .url)
    }

}


