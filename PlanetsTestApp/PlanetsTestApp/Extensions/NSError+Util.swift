//
//  NSError+Util.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import Foundation

extension NSError {
    static func createError(_ code: Int, description: String) -> NSError {
        return NSError(domain: "com.aprearo.TableView",
                       code: 400,
                       userInfo: [
                        "NSLocalizedDescription" : description
            ])
    }
}
