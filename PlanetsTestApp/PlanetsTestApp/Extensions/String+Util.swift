//
//  String+Util.swift
//  PlanetsTestApp
//
//  Created by Aruls Mac on 1/9/19.
//  Copyright © 2019 Planets. All rights reserved.
//

import Foundation

extension String {
    static func emptyIfNil(_ optionalString: String?) -> String {
        let text: String
        if let unwrapped = optionalString {
            text = unwrapped
        } else {
            text = ""
        }
        return text
    }
}
